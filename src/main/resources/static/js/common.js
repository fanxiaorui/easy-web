


function print(data,hideTime){
	hideTime =hideTime?hideTime:2000;
	var $tooltips = $('#js_tooltips');
	$tooltips.html(data);
	$tooltips.css('display', 'block');
	setTimeout(function () {
		$tooltips.css('display', 'none');
    }, hideTime);
}
function doRequest(param,callback){
	type=param.type||'GET';
	$.ajax({ 
		url: param.url,
		type:type,
		async:true,
		headers:param.headers,
		data:param.data,
		timeout:10000,
		dataType:'json', 
		error:function(){
			print('交互错误');
		},success:function(data){
			if(typeof(data.success)=="undefined"){
				callback(data);
				return;
			}
			if(!data.success){
				print(data.msg||'交互错误');
			}else{
				callback(data.data);
			}
		}
      });
}
$.extend({
	  getUrlVars: function(){
	    var vars = [], hash;
	    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
	    for(var i = 0; i < hashes.length; i++)
	    {
	      hash = hashes[i].split('=');
	      vars.push(hash[0]);
	      vars[hash[0]] = hash[1];
	    }
	    return vars;
	  },
	  getUrlVar: function(name){
	    return $.getUrlVars()[name];
	  }
	});
