package com.rogerfan.easy.mapper.sys;

import com.rogerfan.easy.model.sys.ConfigDto;
import com.rogerfan.easy.model.sys.ConfigDtoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.mybatis.generator.gen.GenericDao;

public interface ConfigMapper extends GenericDao {
    long countByExample(ConfigDtoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(ConfigDto record);

    int insertSelective(ConfigDto record);

    List<ConfigDto> selectByExample(ConfigDtoExample example);

    ConfigDto selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") ConfigDto record, @Param("example") ConfigDtoExample example);

    int updateByExample(@Param("record") ConfigDto record, @Param("example") ConfigDtoExample example);

    int updateByPrimaryKeySelective(ConfigDto record);

    int updateByPrimaryKey(ConfigDto record);
}