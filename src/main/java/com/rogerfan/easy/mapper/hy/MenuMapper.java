package com.rogerfan.easy.mapper.hy;

import com.rogerfan.easy.model.hy.MenuDto;
import com.rogerfan.easy.model.hy.MenuDtoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.mybatis.generator.gen.GenericDao;

public interface MenuMapper extends GenericDao {
    long countByExample(MenuDtoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(MenuDto record);

    int insertSelective(MenuDto record);

    List<MenuDto> selectByExample(MenuDtoExample example);

    MenuDto selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") MenuDto record, @Param("example") MenuDtoExample example);

    int updateByExample(@Param("record") MenuDto record, @Param("example") MenuDtoExample example);

    int updateByPrimaryKeySelective(MenuDto record);

    int updateByPrimaryKey(MenuDto record);
}