package com.rogerfan.easy.mapper.hy;

import com.rogerfan.easy.model.hy.RolePurviewDto;
import com.rogerfan.easy.model.hy.RolePurviewDtoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.mybatis.generator.gen.GenericDao;

public interface RolePurviewMapper extends GenericDao {
    long countByExample(RolePurviewDtoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RolePurviewDto record);

    int insertSelective(RolePurviewDto record);

    List<RolePurviewDto> selectByExample(RolePurviewDtoExample example);

    RolePurviewDto selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RolePurviewDto record, @Param("example") RolePurviewDtoExample example);

    int updateByExample(@Param("record") RolePurviewDto record, @Param("example") RolePurviewDtoExample example);

    int updateByPrimaryKeySelective(RolePurviewDto record);

    int updateByPrimaryKey(RolePurviewDto record);
}