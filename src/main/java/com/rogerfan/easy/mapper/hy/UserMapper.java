package com.rogerfan.easy.mapper.hy;

import com.rogerfan.easy.model.hy.UserDto;
import com.rogerfan.easy.model.hy.UserDtoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.mybatis.generator.gen.GenericDao;

public interface UserMapper extends GenericDao {
    long countByExample(UserDtoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserDto record);

    int insertSelective(UserDto record);

    List<UserDto> selectByExample(UserDtoExample example);

    UserDto selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserDto record, @Param("example") UserDtoExample example);

    int updateByExample(@Param("record") UserDto record, @Param("example") UserDtoExample example);

    int updateByPrimaryKeySelective(UserDto record);

    int updateByPrimaryKey(UserDto record);
}