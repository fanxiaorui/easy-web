package com.rogerfan.easy.mapper.hy;

import com.rogerfan.easy.model.hy.RoleDto;
import com.rogerfan.easy.model.hy.RoleDtoExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.mybatis.generator.gen.GenericDao;

public interface RoleMapper extends GenericDao {
    long countByExample(RoleDtoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(RoleDto record);

    int insertSelective(RoleDto record);

    List<RoleDto> selectByExample(RoleDtoExample example);

    RoleDto selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") RoleDto record, @Param("example") RoleDtoExample example);

    int updateByExample(@Param("record") RoleDto record, @Param("example") RoleDtoExample example);

    int updateByPrimaryKeySelective(RoleDto record);

    int updateByPrimaryKey(RoleDto record);
    
    
    public List<RoleDto> selectByUserId(@Param("userid")Long userId) ;
}