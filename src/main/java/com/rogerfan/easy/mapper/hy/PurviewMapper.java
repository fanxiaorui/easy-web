package com.rogerfan.easy.mapper.hy;

import com.rogerfan.easy.model.hy.PurviewDto;
import com.rogerfan.easy.model.hy.PurviewDtoExample;

import java.util.List;

import org.apache.ibatis.annotations.Param;
import org.mybatis.generator.gen.GenericDao;

public interface PurviewMapper extends GenericDao {
    long countByExample(PurviewDtoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(PurviewDto record);

    int insertSelective(PurviewDto record);

    List<PurviewDto> selectByExample(PurviewDtoExample example);

    PurviewDto selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") PurviewDto record, @Param("example") PurviewDtoExample example);

    int updateByExample(@Param("record") PurviewDto record, @Param("example") PurviewDtoExample example);

    int updateByPrimaryKeySelective(PurviewDto record);

    int updateByPrimaryKey(PurviewDto record);
    
    List<PurviewDto>  selectByUserId(@Param("userid")Long userId);
}