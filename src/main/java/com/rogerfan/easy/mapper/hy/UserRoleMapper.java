package com.rogerfan.easy.mapper.hy;

import com.rogerfan.easy.model.hy.UserRoleDto;
import com.rogerfan.easy.model.hy.UserRoleDtoExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;
import org.mybatis.generator.gen.GenericDao;

public interface UserRoleMapper extends GenericDao {
    long countByExample(UserRoleDtoExample example);

    int deleteByPrimaryKey(Long id);

    int insert(UserRoleDto record);

    int insertSelective(UserRoleDto record);

    List<UserRoleDto> selectByExample(UserRoleDtoExample example);

    UserRoleDto selectByPrimaryKey(Long id);

    int updateByExampleSelective(@Param("record") UserRoleDto record, @Param("example") UserRoleDtoExample example);

    int updateByExample(@Param("record") UserRoleDto record, @Param("example") UserRoleDtoExample example);

    int updateByPrimaryKeySelective(UserRoleDto record);

    int updateByPrimaryKey(UserRoleDto record);
}