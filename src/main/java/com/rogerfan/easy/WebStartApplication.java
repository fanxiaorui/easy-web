package com.rogerfan.easy;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@SpringBootApplication
@MapperScan("com.rogerfan.easy.mapper")
@EnableTransactionManagement
@ComponentScan({ "com.rogerfan.easy.**" })
public class WebStartApplication {

	public static void main(String[] args) {
		SpringApplication app = new  SpringApplication(WebStartApplication.class);
		app.setBannerMode(Mode.OFF);
		app.run(args);
	}
}
