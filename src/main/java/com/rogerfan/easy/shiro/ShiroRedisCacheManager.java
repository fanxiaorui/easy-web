package com.rogerfan.easy.shiro;

import org.apache.shiro.cache.AbstractCacheManager;
import org.apache.shiro.cache.Cache;
import org.apache.shiro.cache.CacheException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class ShiroRedisCacheManager extends AbstractCacheManager {

	@Autowired
    private  ShiroRedisCache<String, Object> shiroRedisCache;
	@Override
	protected Cache createCache(String cacheName) throws CacheException {
		return shiroRedisCache;
	}
	public ShiroRedisCache<String, Object> getShiroRedisCache() {
		return shiroRedisCache;
	}
	public void setShiroRedisCache(ShiroRedisCache<String, Object> shiroRedisCache) {
		this.shiroRedisCache = shiroRedisCache;
	}
}