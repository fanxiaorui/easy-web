package com.rogerfan.easy.shiro;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import com.rogerfan.easy.model.hy.UserDto;
import com.rogerfan.easy.service.hy.UserService;

@Component
public class UserCache {

	@Autowired
	UserService userService;
	@Autowired
	RedisTemplate<String, Object> redisTemplate;
	private static String redisKey="shiro_user_cache";
	
	public UserDto save(Long userId){
		UserDto _u =userService.selectByPrimaryKey(userId);
		redisTemplate.opsForHash().put(redisKey,String.valueOf( userId), _u);
		return _u;
	}
	
	public void update(UserDto _u){
		redisTemplate.opsForHash().put(redisKey,String.valueOf(  _u.getId()), _u);
	}
	
	public UserDto get(Long userId){
		Object result =redisTemplate.opsForHash().get(redisKey, String.valueOf(userId));
		if(result == null){
			return save(userId);
		}else{
			return (UserDto)result;
		}
	}
	
	
}
