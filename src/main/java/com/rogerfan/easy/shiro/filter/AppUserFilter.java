package com.rogerfan.easy.shiro.filter;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.filter.authc.UserFilter;
import org.springframework.stereotype.Component;

import com.rogerfan.easy.web.util.WebUtil;

@Component
public class AppUserFilter extends UserFilter{

	@Override
	protected boolean isAccessAllowed(ServletRequest request,
			ServletResponse response, Object mappedValue) {
		// TODO Auto-generated method stub
		return super.isAccessAllowed(request, response, mappedValue);
	}

	/**
	 * 取不到用户数据时触发
	 */
	@Override
	protected boolean onAccessDenied(ServletRequest request,
			ServletResponse response) throws Exception {
		if (!WebUtil.isAjaxRequest(request)){
			return super.onAccessDenied(request, response);
		} else {
			try {
				WebUtil.print(response, WebUtil.noUser);
			} catch (IOException e1) {
				
			}
			return false;
		}
	}

}
