package com.rogerfan.easy.shiro.filter;

import java.io.IOException;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.apache.shiro.web.filter.authz.PermissionsAuthorizationFilter;
import org.springframework.stereotype.Component;

import com.rogerfan.easy.web.util.WebUtil;

@Component
public class AppPermissionAuthorizationFilter extends PermissionsAuthorizationFilter{

	@Override
	protected boolean onAccessDenied(ServletRequest request,
			ServletResponse response) throws IOException {
		if(!WebUtil.isAjaxRequest(request)){
			return super.onAccessDenied(request, response);
		}else{
			WebUtil.print(response, WebUtil.unAuth);
			return false;
		}
		
	}

	
}
