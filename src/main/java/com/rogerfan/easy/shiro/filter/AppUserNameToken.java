package com.rogerfan.easy.shiro.filter;

import org.apache.shiro.authc.UsernamePasswordToken;

public class AppUserNameToken extends UsernamePasswordToken {

	
	public static String REQUEST_TOKEN="REQUEST_APP_TOKEN";
	public AppUserNameToken(final String username, final String password,
			final boolean rememberMe, final String host) {
		super(username, password != null ? password.toCharArray() : null,
				rememberMe, host);
	}

	private String token;

	private boolean isAppLogin;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public boolean isAppLogin() {
		return isAppLogin;
	}

	public void setAppLogin(boolean isAppLogin) {
		this.isAppLogin = isAppLogin;
	}

}
