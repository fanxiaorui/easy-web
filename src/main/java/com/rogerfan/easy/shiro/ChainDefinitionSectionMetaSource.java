package com.rogerfan.easy.shiro;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.config.Ini;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.collect.Maps;
import com.rogerfan.easy.model.hy.PurviewDto;
import com.rogerfan.easy.service.hy.PurviewService;

@Component
public class ChainDefinitionSectionMetaSource {
	private static final Logger log = LoggerFactory
			.getLogger(ChainDefinitionSectionMetaSource.class);

	@Autowired
	PurviewService secPurviewService;

	/**
	 * 默认premission字符串
	 */
	public static final String PREMISSION_STRING = "perms[\"{0}\"]";

	public Map<String, String> getMetaSouce() throws BeansException {
		// 获取所有资源
		Ini ini = new Ini();
		// 加载默认的url
		// ini.load(filterChainDefinitions);
		Map<String, String> section =Maps.newLinkedHashMap();

		Map<String, List<String>> purviews = new HashMap<String, List<String>>();

		try {
			List<PurviewDto> _PurviewVOList = secPurviewService
					.selectByExample(null);
			for (PurviewDto pur : _PurviewVOList) {
				String purviewurl = pur.getUrlPath();
				if (purviewurl.contains(";")) {
					String[] temp = pur.getUrlPath().split(";");
					for (int i = 0; i < temp.length; i++) {
						if (StringUtils.isNotEmpty(temp[i])
								&& StringUtils.isNotEmpty(pur.getPurCode())) {
							if (purviews.containsKey(temp[i])) {
								purviews.get(temp[i]).add(pur.getPurCode());
							} else {
								List<String> purCodes = new ArrayList<String>();
								purCodes.add(pur.getPurCode());
								purviews.put(temp[i], purCodes);
							}
						}
					}
				} else {
					if (purviews.containsKey(purviewurl)) {
						purviews.get(purviewurl).add(pur.getPurCode());
					} else {
						List<String> purCodes = new ArrayList<String>();
						purCodes.add(pur.getPurCode());
						purviews.put(purviewurl, purCodes);
					}
				}
			}

			for (String key : purviews.keySet()) {
				section.put(
						key,
						"user,"
								+ MessageFormat
										.format(PREMISSION_STRING,
												StringUtils.join(
														purviews.get(key), ',')));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
		return section;
	}

	// public void updatePermission() {
	//
	//
	//
	// AbstractShiroFilter shiroFilter = null;
	//
	// try {
	// shiroFilter = (AbstractShiroFilter) shiroFilterFactoryBean
	// .getObject();
	// } catch (Exception e) {
	// log.error(e.getMessage());
	// }
	//
	// // 获取过滤管理器
	// PathMatchingFilterChainResolver filterChainResolver =
	// (PathMatchingFilterChainResolver) shiroFilter
	// .getFilterChainResolver();
	// DefaultFilterChainManager manager = (DefaultFilterChainManager)
	// filterChainResolver
	// .getFilterChainManager();
	//
	// // 清空初始权限配置
	// manager.getFilterChains().clear();
	// shiroFilterFactoryBean.getFilterChainDefinitionMap().clear();
	//
	// // 重新构建生成
	// shiroFilterFactoryBean
	// .setFilterChainDefinitions(filterChainDefinitions);
	// Map<String, String> chains = shiroFilterFactoryBean
	// .getFilterChainDefinitionMap();
	//
	// for (Map.Entry<String, String> entry : chains.entrySet()) {
	// String url = entry.getKey();
	// String chainDefinition = entry.getValue().trim()
	// .replace(" ", "");
	// manager.createChain(url, chainDefinition);
	// }
	//
	// Map<String, List<String>> purviews = new HashMap<String, List<String>>();
	//
	// List<PurviewDto> _PurviewVOList = secPurviewService
	// .selectByExample(null);
	// for (PurviewDto pur : _PurviewVOList) {
	// String purviewurl = pur.getUrlPath();
	// if (purviewurl.contains(";")) {
	// String[] temp = pur.getUrlPath().split(";");
	// for (int i = 0; i < temp.length; i++) {
	// if (StringUtils.isNotEmpty(temp[i])
	// && StringUtils.isNotEmpty(pur.getPurCode())) {
	// if (purviews.containsKey(temp[i])) {
	// purviews.get(temp[i]).add(pur.getPurCode());
	// } else {
	// List<String> purCodes = new ArrayList<String>();
	// purCodes.add(pur.getPurCode());
	// purviews.put(temp[i], purCodes);
	// }
	// }
	// }
	// } else {
	// if (purviews.containsKey(purviewurl)) {
	// purviews.get(purviewurl).add(pur.getPurCode());
	// } else {
	// List<String> purCodes = new ArrayList<String>();
	// purCodes.add(pur.getPurCode());
	// purviews.put(purviewurl, purCodes);
	// }
	// }
	// }
	//
	// for (String key : purviews.keySet()) {
	// manager.createChain(
	// key,
	// "user,concurrentlogin,"
	// + MessageFormat
	// .format(PREMISSION_STRING,
	// StringUtils.join(
	// purviews.get(key), ',')));
	// }
	//
	//
	// log.debug("update shiro permission success...");
	// }
	//
	



}