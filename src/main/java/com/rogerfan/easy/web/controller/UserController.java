package com.rogerfan.easy.web.controller;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authc.UnknownAccountException;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.rogerfan.easy.common.R;
import com.rogerfan.easy.model.hy.UserDto;
import com.rogerfan.easy.shiro.ShiroUtils;
import com.rogerfan.easy.shiro.filter.AppUserNameToken;
import com.rogerfan.easy.web.config.resolver.LoginUser;
import com.rogerfan.easy.web.util.WebUtil;

@Controller
@RequestMapping(value = "/user")
public class UserController {

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String login(HttpServletRequest req,HttpServletResponse response, Model model) throws IOException{
		String exceptionClassName = (String)req.getAttribute("shiroLoginFailure");
		if(StringUtils.isNoneBlank(exceptionClassName)){
			String error = null;
	        if(UnknownAccountException.class.getName().equals(exceptionClassName)) {
	            error = "用户名/密码错误";
	        }
	        model.addAttribute("error", error);
		}
		
		AppUserNameToken token = ShiroUtils.getToken();
		if(token!=null &&token.isAppLogin()){
			WebUtil.print(response, R.ok(token.getToken()));
		}
          
        return "user";
	}
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String toLogin(){
		return "user";
	}
	@RequestMapping(value = "/tologinApp", method = RequestMethod.GET)
	public String tologinApp(){
		return "userapp";
	}
	@RequestMapping(value = "/center", method = RequestMethod.GET)
	public String center(@LoginUser UserDto user){
		System.out.println(user);
		return "center";
	}
}
