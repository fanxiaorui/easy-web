package com.rogerfan.easy.service.sys;

import com.rogerfan.easy.model.sys.ConfigDto;
import com.rogerfan.easy.model.sys.ConfigDtoExample;
import java.util.List;

public interface ConfigService extends org.mybatis.generator.gen.GenericService<ConfigDto, Long> {
    List<ConfigDto> selectByExample(ConfigDtoExample e);

    ConfigDto selectOneByExample(ConfigDtoExample e);

    int updateByExample(ConfigDto record, ConfigDtoExample e);

    int updateByExampleSelective(ConfigDto record, ConfigDtoExample e);
}