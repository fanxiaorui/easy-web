package com.rogerfan.easy.service.sys.impl;

import com.rogerfan.easy.mapper.sys.ConfigMapper;
import com.rogerfan.easy.model.sys.ConfigDto;
import com.rogerfan.easy.model.sys.ConfigDtoExample;
import com.rogerfan.easy.service.sys.ConfigService;
import java.util.List;
import org.mybatis.generator.gen.GenericDao;
import org.mybatis.generator.gen.GenericServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class ConfigServiceImpl extends GenericServiceImpl<ConfigDto, Long> implements ConfigService {
    @Autowired
    private ConfigMapper configMapper;

    public static final Logger logger = LoggerFactory.getLogger(ConfigServiceImpl.class);

    @Override
    @SuppressWarnings("unchecked")
    public GenericDao<ConfigDto, Long> getDao() {
        return configMapper;
    }

    @Override
    public List<ConfigDto> selectByExample(ConfigDtoExample e) {
        return configMapper.selectByExample(e);
    }

    @Override
    public ConfigDto selectOneByExample(ConfigDtoExample e) {
        List<com.rogerfan.easy.model.sys.ConfigDto> lists = this.configMapper.selectByExample(e);
        return (lists==null||lists.isEmpty())?null:lists.get(0);
    }

    @Override
    public int updateByExample(ConfigDto record, ConfigDtoExample e) {
        return configMapper.updateByExample(record,e);
    }

    @Override
    public int updateByExampleSelective(ConfigDto record, ConfigDtoExample e) {
        return configMapper.updateByExampleSelective(record,e);
    }
}