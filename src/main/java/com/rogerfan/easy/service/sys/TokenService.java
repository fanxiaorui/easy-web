package com.rogerfan.easy.service.sys;

import java.util.concurrent.TimeUnit;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import com.rogerfan.easy.common.IdFactory;
import com.rogerfan.easy.model.hy.UserDto;

@Component
public class TokenService {
	
	@Autowired
	RedisTemplate<String, Object> redisTemplate;
	public String genToken(UserDto user){
		String token =IdFactory.getRandomSerialNumber();
		redisTemplate.opsForValue().set(token, user, 60*60l,TimeUnit.SECONDS);
		return token;
	}
}
