package com.rogerfan.easy.service.hy;

import com.rogerfan.easy.model.hy.UserRoleDto;
import com.rogerfan.easy.model.hy.UserRoleDtoExample;
import java.util.List;

public interface UserRoleService extends org.mybatis.generator.gen.GenericService<UserRoleDto, Long> {
    List<UserRoleDto> selectByExample(UserRoleDtoExample e);

    UserRoleDto selectOneByExample(UserRoleDtoExample e);

    int updateByExample(UserRoleDto record, UserRoleDtoExample e);

    int updateByExampleSelective(UserRoleDto record, UserRoleDtoExample e);
}