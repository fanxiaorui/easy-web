package com.rogerfan.easy.service.hy.impl;

import com.rogerfan.easy.mapper.hy.PurviewMapper;
import com.rogerfan.easy.model.hy.PurviewDto;
import com.rogerfan.easy.model.hy.PurviewDtoExample;
import com.rogerfan.easy.service.hy.PurviewService;

import java.util.List;

import org.mybatis.generator.gen.GenericDao;
import org.mybatis.generator.gen.GenericServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PurviewServiceImpl extends GenericServiceImpl<PurviewDto, Long> implements PurviewService {
    @Autowired
    private PurviewMapper purviewMapper;

    public static final Logger logger = LoggerFactory.getLogger(PurviewServiceImpl.class);

    @Override
    @SuppressWarnings("unchecked")
    public GenericDao<PurviewDto, Long> getDao() {
        return purviewMapper;
    }

    @Override
    public List<PurviewDto> selectByExample(PurviewDtoExample e) {
        return purviewMapper.selectByExample(e);
    }

    @Override
    public PurviewDto selectOneByExample(PurviewDtoExample e) {
        List<com.rogerfan.easy.model.hy.PurviewDto> lists = this.purviewMapper.selectByExample(e);
        return (lists==null||lists.isEmpty())?null:lists.get(0);
    }

    @Override
    public int updateByExample(PurviewDto record, PurviewDtoExample e) {
        return purviewMapper.updateByExample(record,e);
    }

    @Override
    public int updateByExampleSelective(PurviewDto record, PurviewDtoExample e) {
        return purviewMapper.updateByExampleSelective(record,e);
    }

	@Override
	public List<PurviewDto> selectByUserId(Long userId) {
		return purviewMapper.selectByUserId(userId);
	}
}