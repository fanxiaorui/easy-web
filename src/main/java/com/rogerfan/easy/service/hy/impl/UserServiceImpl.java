package com.rogerfan.easy.service.hy.impl;

import com.rogerfan.easy.mapper.hy.UserMapper;
import com.rogerfan.easy.model.hy.UserDto;
import com.rogerfan.easy.model.hy.UserDtoExample;
import com.rogerfan.easy.service.hy.UserService;
import java.util.List;
import org.mybatis.generator.gen.GenericDao;
import org.mybatis.generator.gen.GenericServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserServiceImpl extends GenericServiceImpl<UserDto, Long> implements UserService {
    @Autowired
    private UserMapper userMapper;

    public static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    @SuppressWarnings("unchecked")
    public GenericDao<UserDto, Long> getDao() {
        return userMapper;
    }

    @Override
    public List<UserDto> selectByExample(UserDtoExample e) {
        return userMapper.selectByExample(e);
    }

    @Override
    public UserDto selectOneByExample(UserDtoExample e) {
        List<com.rogerfan.easy.model.hy.UserDto> lists = this.userMapper.selectByExample(e);
        return (lists==null||lists.isEmpty())?null:lists.get(0);
    }

    @Override
    public int updateByExample(UserDto record, UserDtoExample e) {
        return userMapper.updateByExample(record,e);
    }

    @Override
    public int updateByExampleSelective(UserDto record, UserDtoExample e) {
        return userMapper.updateByExampleSelective(record,e);
    }
}