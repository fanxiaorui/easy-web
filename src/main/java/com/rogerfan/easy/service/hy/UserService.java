package com.rogerfan.easy.service.hy;

import com.rogerfan.easy.model.hy.UserDto;
import com.rogerfan.easy.model.hy.UserDtoExample;
import java.util.List;

public interface UserService extends org.mybatis.generator.gen.GenericService<UserDto, Long> {
    List<UserDto> selectByExample(UserDtoExample e);

    UserDto selectOneByExample(UserDtoExample e);

    int updateByExample(UserDto record, UserDtoExample e);

    int updateByExampleSelective(UserDto record, UserDtoExample e);
}