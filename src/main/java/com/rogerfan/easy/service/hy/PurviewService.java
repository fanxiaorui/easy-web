package com.rogerfan.easy.service.hy;

import java.util.List;

import com.rogerfan.easy.model.hy.PurviewDto;
import com.rogerfan.easy.model.hy.PurviewDtoExample;

public interface PurviewService extends org.mybatis.generator.gen.GenericService<PurviewDto, Long> {
    List<PurviewDto> selectByExample(PurviewDtoExample e);

    PurviewDto selectOneByExample(PurviewDtoExample e);

    int updateByExample(PurviewDto record, PurviewDtoExample e);

    int updateByExampleSelective(PurviewDto record, PurviewDtoExample e);
    
    List<PurviewDto>  selectByUserId(Long userId);
}