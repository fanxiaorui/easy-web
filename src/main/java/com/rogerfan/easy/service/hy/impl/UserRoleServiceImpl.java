package com.rogerfan.easy.service.hy.impl;

import com.rogerfan.easy.mapper.hy.UserRoleMapper;
import com.rogerfan.easy.model.hy.UserRoleDto;
import com.rogerfan.easy.model.hy.UserRoleDtoExample;
import com.rogerfan.easy.service.hy.UserRoleService;
import java.util.List;
import org.mybatis.generator.gen.GenericDao;
import org.mybatis.generator.gen.GenericServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class UserRoleServiceImpl extends GenericServiceImpl<UserRoleDto, Long> implements UserRoleService {
    @Autowired
    private UserRoleMapper userRoleMapper;

    public static final Logger logger = LoggerFactory.getLogger(UserRoleServiceImpl.class);

    @Override
    @SuppressWarnings("unchecked")
    public GenericDao<UserRoleDto, Long> getDao() {
        return userRoleMapper;
    }

    @Override
    public List<UserRoleDto> selectByExample(UserRoleDtoExample e) {
        return userRoleMapper.selectByExample(e);
    }

    @Override
    public UserRoleDto selectOneByExample(UserRoleDtoExample e) {
        List<com.rogerfan.easy.model.hy.UserRoleDto> lists = this.userRoleMapper.selectByExample(e);
        return (lists==null||lists.isEmpty())?null:lists.get(0);
    }

    @Override
    public int updateByExample(UserRoleDto record, UserRoleDtoExample e) {
        return userRoleMapper.updateByExample(record,e);
    }

    @Override
    public int updateByExampleSelective(UserRoleDto record, UserRoleDtoExample e) {
        return userRoleMapper.updateByExampleSelective(record,e);
    }
}