package com.rogerfan.easy.service.hy;

import com.rogerfan.easy.model.hy.RoleDto;
import com.rogerfan.easy.model.hy.RoleDtoExample;
import java.util.List;

public interface RoleService extends org.mybatis.generator.gen.GenericService<RoleDto, Long> {
    List<RoleDto> selectByExample(RoleDtoExample e);

    RoleDto selectOneByExample(RoleDtoExample e);

    int updateByExample(RoleDto record, RoleDtoExample e);

    int updateByExampleSelective(RoleDto record, RoleDtoExample e);
    
    List<RoleDto> selectByUserId(Long userId);
}