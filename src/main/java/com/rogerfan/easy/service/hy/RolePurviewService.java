package com.rogerfan.easy.service.hy;

import com.rogerfan.easy.model.hy.RolePurviewDto;
import com.rogerfan.easy.model.hy.RolePurviewDtoExample;
import java.util.List;

public interface RolePurviewService extends org.mybatis.generator.gen.GenericService<RolePurviewDto, Long> {
    List<RolePurviewDto> selectByExample(RolePurviewDtoExample e);

    RolePurviewDto selectOneByExample(RolePurviewDtoExample e);

    int updateByExample(RolePurviewDto record, RolePurviewDtoExample e);

    int updateByExampleSelective(RolePurviewDto record, RolePurviewDtoExample e);
}