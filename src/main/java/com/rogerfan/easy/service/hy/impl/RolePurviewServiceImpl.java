package com.rogerfan.easy.service.hy.impl;

import com.rogerfan.easy.mapper.hy.RolePurviewMapper;
import com.rogerfan.easy.model.hy.RolePurviewDto;
import com.rogerfan.easy.model.hy.RolePurviewDtoExample;
import com.rogerfan.easy.service.hy.RolePurviewService;
import java.util.List;
import org.mybatis.generator.gen.GenericDao;
import org.mybatis.generator.gen.GenericServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RolePurviewServiceImpl extends GenericServiceImpl<RolePurviewDto, Long> implements RolePurviewService {
    @Autowired
    private RolePurviewMapper rolePurviewMapper;

    public static final Logger logger = LoggerFactory.getLogger(RolePurviewServiceImpl.class);

    @Override
    @SuppressWarnings("unchecked")
    public GenericDao<RolePurviewDto, Long> getDao() {
        return rolePurviewMapper;
    }

    @Override
    public List<RolePurviewDto> selectByExample(RolePurviewDtoExample e) {
        return rolePurviewMapper.selectByExample(e);
    }

    @Override
    public RolePurviewDto selectOneByExample(RolePurviewDtoExample e) {
        List<com.rogerfan.easy.model.hy.RolePurviewDto> lists = this.rolePurviewMapper.selectByExample(e);
        return (lists==null||lists.isEmpty())?null:lists.get(0);
    }

    @Override
    public int updateByExample(RolePurviewDto record, RolePurviewDtoExample e) {
        return rolePurviewMapper.updateByExample(record,e);
    }

    @Override
    public int updateByExampleSelective(RolePurviewDto record, RolePurviewDtoExample e) {
        return rolePurviewMapper.updateByExampleSelective(record,e);
    }
}