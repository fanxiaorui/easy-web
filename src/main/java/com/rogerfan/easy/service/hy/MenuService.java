package com.rogerfan.easy.service.hy;

import com.rogerfan.easy.model.hy.MenuDto;
import com.rogerfan.easy.model.hy.MenuDtoExample;
import java.util.List;

public interface MenuService extends org.mybatis.generator.gen.GenericService<MenuDto, Long> {
    List<MenuDto> selectByExample(MenuDtoExample e);

    MenuDto selectOneByExample(MenuDtoExample e);

    int updateByExample(MenuDto record, MenuDtoExample e);

    int updateByExampleSelective(MenuDto record, MenuDtoExample e);
}