package com.rogerfan.easy.service.hy.impl;

import com.rogerfan.easy.mapper.hy.RoleMapper;
import com.rogerfan.easy.model.hy.RoleDto;
import com.rogerfan.easy.model.hy.RoleDtoExample;
import com.rogerfan.easy.service.hy.RoleService;

import java.util.List;

import org.mybatis.generator.gen.GenericDao;
import org.mybatis.generator.gen.GenericServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RoleServiceImpl extends GenericServiceImpl<RoleDto, Long> implements RoleService {
    @Autowired
    private RoleMapper roleMapper;

    public static final Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);

    @Override
    @SuppressWarnings("unchecked")
    public GenericDao<RoleDto, Long> getDao() {
        return roleMapper;
    }

    @Override
    public List<RoleDto> selectByExample(RoleDtoExample e) {
        return roleMapper.selectByExample(e);
    }

    @Override
    public RoleDto selectOneByExample(RoleDtoExample e) {
        List<com.rogerfan.easy.model.hy.RoleDto> lists = this.roleMapper.selectByExample(e);
        return (lists==null||lists.isEmpty())?null:lists.get(0);
    }

    @Override
    public int updateByExample(RoleDto record, RoleDtoExample e) {
        return roleMapper.updateByExample(record,e);
    }

    @Override
    public int updateByExampleSelective(RoleDto record, RoleDtoExample e) {
        return roleMapper.updateByExampleSelective(record,e);
    }

	@Override
	public List<RoleDto> selectByUserId(Long userId) {
		return roleMapper.selectByUserId(userId);
	}
}