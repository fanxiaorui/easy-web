package com.rogerfan.easy.service.hy.impl;

import com.rogerfan.easy.mapper.hy.MenuMapper;
import com.rogerfan.easy.model.hy.MenuDto;
import com.rogerfan.easy.model.hy.MenuDtoExample;
import com.rogerfan.easy.service.hy.MenuService;
import java.util.List;
import org.mybatis.generator.gen.GenericDao;
import org.mybatis.generator.gen.GenericServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MenuServiceImpl extends GenericServiceImpl<MenuDto, Long> implements MenuService {
    @Autowired
    private MenuMapper menuMapper;

    public static final Logger logger = LoggerFactory.getLogger(MenuServiceImpl.class);

    @Override
    @SuppressWarnings("unchecked")
    public GenericDao<MenuDto, Long> getDao() {
        return menuMapper;
    }

    @Override
    public List<MenuDto> selectByExample(MenuDtoExample e) {
        return menuMapper.selectByExample(e);
    }

    @Override
    public MenuDto selectOneByExample(MenuDtoExample e) {
        List<com.rogerfan.easy.model.hy.MenuDto> lists = this.menuMapper.selectByExample(e);
        return (lists==null||lists.isEmpty())?null:lists.get(0);
    }

    @Override
    public int updateByExample(MenuDto record, MenuDtoExample e) {
        return menuMapper.updateByExample(record,e);
    }

    @Override
    public int updateByExampleSelective(MenuDto record, MenuDtoExample e) {
        return menuMapper.updateByExampleSelective(record,e);
    }
}