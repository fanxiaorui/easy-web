package com.rogerfan.easy.model.hy;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class PurviewDtoExample {
    protected String orderByClause;

    protected boolean distinct;

    protected List<Criteria> oredCriteria;

    public PurviewDtoExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    public String getOrderByClause() {
        return orderByClause;
    }

    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    public boolean isDistinct() {
        return distinct;
    }

    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andIdIsNull() {
            addCriterion("id is null");
            return (Criteria) this;
        }

        public Criteria andIdIsNotNull() {
            addCriterion("id is not null");
            return (Criteria) this;
        }

        public Criteria andIdEqualTo(Long value) {
            addCriterion("id =", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotEqualTo(Long value) {
            addCriterion("id <>", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThan(Long value) {
            addCriterion("id >", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdGreaterThanOrEqualTo(Long value) {
            addCriterion("id >=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThan(Long value) {
            addCriterion("id <", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdLessThanOrEqualTo(Long value) {
            addCriterion("id <=", value, "id");
            return (Criteria) this;
        }

        public Criteria andIdIn(List<Long> values) {
            addCriterion("id in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotIn(List<Long> values) {
            addCriterion("id not in", values, "id");
            return (Criteria) this;
        }

        public Criteria andIdBetween(Long value1, Long value2) {
            addCriterion("id between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andIdNotBetween(Long value1, Long value2) {
            addCriterion("id not between", value1, value2, "id");
            return (Criteria) this;
        }

        public Criteria andPurCodeIsNull() {
            addCriterion("pur_code is null");
            return (Criteria) this;
        }

        public Criteria andPurCodeIsNotNull() {
            addCriterion("pur_code is not null");
            return (Criteria) this;
        }

        public Criteria andPurCodeEqualTo(String value) {
            addCriterion("pur_code =", value, "purCode");
            return (Criteria) this;
        }

        public Criteria andPurCodeNotEqualTo(String value) {
            addCriterion("pur_code <>", value, "purCode");
            return (Criteria) this;
        }

        public Criteria andPurCodeGreaterThan(String value) {
            addCriterion("pur_code >", value, "purCode");
            return (Criteria) this;
        }

        public Criteria andPurCodeGreaterThanOrEqualTo(String value) {
            addCriterion("pur_code >=", value, "purCode");
            return (Criteria) this;
        }

        public Criteria andPurCodeLessThan(String value) {
            addCriterion("pur_code <", value, "purCode");
            return (Criteria) this;
        }

        public Criteria andPurCodeLessThanOrEqualTo(String value) {
            addCriterion("pur_code <=", value, "purCode");
            return (Criteria) this;
        }

        public Criteria andPurCodeLike(String value) {
            addCriterion("pur_code like", value, "purCode");
            return (Criteria) this;
        }

        public Criteria andPurCodeNotLike(String value) {
            addCriterion("pur_code not like", value, "purCode");
            return (Criteria) this;
        }

        public Criteria andPurCodeIn(List<String> values) {
            addCriterion("pur_code in", values, "purCode");
            return (Criteria) this;
        }

        public Criteria andPurCodeNotIn(List<String> values) {
            addCriterion("pur_code not in", values, "purCode");
            return (Criteria) this;
        }

        public Criteria andPurCodeBetween(String value1, String value2) {
            addCriterion("pur_code between", value1, value2, "purCode");
            return (Criteria) this;
        }

        public Criteria andPurCodeNotBetween(String value1, String value2) {
            addCriterion("pur_code not between", value1, value2, "purCode");
            return (Criteria) this;
        }

        public Criteria andPurNameIsNull() {
            addCriterion("pur_name is null");
            return (Criteria) this;
        }

        public Criteria andPurNameIsNotNull() {
            addCriterion("pur_name is not null");
            return (Criteria) this;
        }

        public Criteria andPurNameEqualTo(String value) {
            addCriterion("pur_name =", value, "purName");
            return (Criteria) this;
        }

        public Criteria andPurNameNotEqualTo(String value) {
            addCriterion("pur_name <>", value, "purName");
            return (Criteria) this;
        }

        public Criteria andPurNameGreaterThan(String value) {
            addCriterion("pur_name >", value, "purName");
            return (Criteria) this;
        }

        public Criteria andPurNameGreaterThanOrEqualTo(String value) {
            addCriterion("pur_name >=", value, "purName");
            return (Criteria) this;
        }

        public Criteria andPurNameLessThan(String value) {
            addCriterion("pur_name <", value, "purName");
            return (Criteria) this;
        }

        public Criteria andPurNameLessThanOrEqualTo(String value) {
            addCriterion("pur_name <=", value, "purName");
            return (Criteria) this;
        }

        public Criteria andPurNameLike(String value) {
            addCriterion("pur_name like", value, "purName");
            return (Criteria) this;
        }

        public Criteria andPurNameNotLike(String value) {
            addCriterion("pur_name not like", value, "purName");
            return (Criteria) this;
        }

        public Criteria andPurNameIn(List<String> values) {
            addCriterion("pur_name in", values, "purName");
            return (Criteria) this;
        }

        public Criteria andPurNameNotIn(List<String> values) {
            addCriterion("pur_name not in", values, "purName");
            return (Criteria) this;
        }

        public Criteria andPurNameBetween(String value1, String value2) {
            addCriterion("pur_name between", value1, value2, "purName");
            return (Criteria) this;
        }

        public Criteria andPurNameNotBetween(String value1, String value2) {
            addCriterion("pur_name not between", value1, value2, "purName");
            return (Criteria) this;
        }

        public Criteria andParentNameIsNull() {
            addCriterion("parent_name is null");
            return (Criteria) this;
        }

        public Criteria andParentNameIsNotNull() {
            addCriterion("parent_name is not null");
            return (Criteria) this;
        }

        public Criteria andParentNameEqualTo(String value) {
            addCriterion("parent_name =", value, "parentName");
            return (Criteria) this;
        }

        public Criteria andParentNameNotEqualTo(String value) {
            addCriterion("parent_name <>", value, "parentName");
            return (Criteria) this;
        }

        public Criteria andParentNameGreaterThan(String value) {
            addCriterion("parent_name >", value, "parentName");
            return (Criteria) this;
        }

        public Criteria andParentNameGreaterThanOrEqualTo(String value) {
            addCriterion("parent_name >=", value, "parentName");
            return (Criteria) this;
        }

        public Criteria andParentNameLessThan(String value) {
            addCriterion("parent_name <", value, "parentName");
            return (Criteria) this;
        }

        public Criteria andParentNameLessThanOrEqualTo(String value) {
            addCriterion("parent_name <=", value, "parentName");
            return (Criteria) this;
        }

        public Criteria andParentNameLike(String value) {
            addCriterion("parent_name like", value, "parentName");
            return (Criteria) this;
        }

        public Criteria andParentNameNotLike(String value) {
            addCriterion("parent_name not like", value, "parentName");
            return (Criteria) this;
        }

        public Criteria andParentNameIn(List<String> values) {
            addCriterion("parent_name in", values, "parentName");
            return (Criteria) this;
        }

        public Criteria andParentNameNotIn(List<String> values) {
            addCriterion("parent_name not in", values, "parentName");
            return (Criteria) this;
        }

        public Criteria andParentNameBetween(String value1, String value2) {
            addCriterion("parent_name between", value1, value2, "parentName");
            return (Criteria) this;
        }

        public Criteria andParentNameNotBetween(String value1, String value2) {
            addCriterion("parent_name not between", value1, value2, "parentName");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNull() {
            addCriterion("parent_id is null");
            return (Criteria) this;
        }

        public Criteria andParentIdIsNotNull() {
            addCriterion("parent_id is not null");
            return (Criteria) this;
        }

        public Criteria andParentIdEqualTo(Long value) {
            addCriterion("parent_id =", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotEqualTo(Long value) {
            addCriterion("parent_id <>", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThan(Long value) {
            addCriterion("parent_id >", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdGreaterThanOrEqualTo(Long value) {
            addCriterion("parent_id >=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThan(Long value) {
            addCriterion("parent_id <", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdLessThanOrEqualTo(Long value) {
            addCriterion("parent_id <=", value, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdIn(List<Long> values) {
            addCriterion("parent_id in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotIn(List<Long> values) {
            addCriterion("parent_id not in", values, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdBetween(Long value1, Long value2) {
            addCriterion("parent_id between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andParentIdNotBetween(Long value1, Long value2) {
            addCriterion("parent_id not between", value1, value2, "parentId");
            return (Criteria) this;
        }

        public Criteria andUrlPathIsNull() {
            addCriterion("url_path is null");
            return (Criteria) this;
        }

        public Criteria andUrlPathIsNotNull() {
            addCriterion("url_path is not null");
            return (Criteria) this;
        }

        public Criteria andUrlPathEqualTo(String value) {
            addCriterion("url_path =", value, "urlPath");
            return (Criteria) this;
        }

        public Criteria andUrlPathNotEqualTo(String value) {
            addCriterion("url_path <>", value, "urlPath");
            return (Criteria) this;
        }

        public Criteria andUrlPathGreaterThan(String value) {
            addCriterion("url_path >", value, "urlPath");
            return (Criteria) this;
        }

        public Criteria andUrlPathGreaterThanOrEqualTo(String value) {
            addCriterion("url_path >=", value, "urlPath");
            return (Criteria) this;
        }

        public Criteria andUrlPathLessThan(String value) {
            addCriterion("url_path <", value, "urlPath");
            return (Criteria) this;
        }

        public Criteria andUrlPathLessThanOrEqualTo(String value) {
            addCriterion("url_path <=", value, "urlPath");
            return (Criteria) this;
        }

        public Criteria andUrlPathLike(String value) {
            addCriterion("url_path like", value, "urlPath");
            return (Criteria) this;
        }

        public Criteria andUrlPathNotLike(String value) {
            addCriterion("url_path not like", value, "urlPath");
            return (Criteria) this;
        }

        public Criteria andUrlPathIn(List<String> values) {
            addCriterion("url_path in", values, "urlPath");
            return (Criteria) this;
        }

        public Criteria andUrlPathNotIn(List<String> values) {
            addCriterion("url_path not in", values, "urlPath");
            return (Criteria) this;
        }

        public Criteria andUrlPathBetween(String value1, String value2) {
            addCriterion("url_path between", value1, value2, "urlPath");
            return (Criteria) this;
        }

        public Criteria andUrlPathNotBetween(String value1, String value2) {
            addCriterion("url_path not between", value1, value2, "urlPath");
            return (Criteria) this;
        }

        public Criteria andDesIsNull() {
            addCriterion("des is null");
            return (Criteria) this;
        }

        public Criteria andDesIsNotNull() {
            addCriterion("des is not null");
            return (Criteria) this;
        }

        public Criteria andDesEqualTo(String value) {
            addCriterion("des =", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesNotEqualTo(String value) {
            addCriterion("des <>", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesGreaterThan(String value) {
            addCriterion("des >", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesGreaterThanOrEqualTo(String value) {
            addCriterion("des >=", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesLessThan(String value) {
            addCriterion("des <", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesLessThanOrEqualTo(String value) {
            addCriterion("des <=", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesLike(String value) {
            addCriterion("des like", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesNotLike(String value) {
            addCriterion("des not like", value, "des");
            return (Criteria) this;
        }

        public Criteria andDesIn(List<String> values) {
            addCriterion("des in", values, "des");
            return (Criteria) this;
        }

        public Criteria andDesNotIn(List<String> values) {
            addCriterion("des not in", values, "des");
            return (Criteria) this;
        }

        public Criteria andDesBetween(String value1, String value2) {
            addCriterion("des between", value1, value2, "des");
            return (Criteria) this;
        }

        public Criteria andDesNotBetween(String value1, String value2) {
            addCriterion("des not between", value1, value2, "des");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNull() {
            addCriterion("add_time is null");
            return (Criteria) this;
        }

        public Criteria andAddTimeIsNotNull() {
            addCriterion("add_time is not null");
            return (Criteria) this;
        }

        public Criteria andAddTimeEqualTo(Date value) {
            addCriterion("add_time =", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotEqualTo(Date value) {
            addCriterion("add_time <>", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThan(Date value) {
            addCriterion("add_time >", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeGreaterThanOrEqualTo(Date value) {
            addCriterion("add_time >=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThan(Date value) {
            addCriterion("add_time <", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeLessThanOrEqualTo(Date value) {
            addCriterion("add_time <=", value, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeIn(List<Date> values) {
            addCriterion("add_time in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotIn(List<Date> values) {
            addCriterion("add_time not in", values, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeBetween(Date value1, Date value2) {
            addCriterion("add_time between", value1, value2, "addTime");
            return (Criteria) this;
        }

        public Criteria andAddTimeNotBetween(Date value1, Date value2) {
            addCriterion("add_time not between", value1, value2, "addTime");
            return (Criteria) this;
        }
    }

    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}