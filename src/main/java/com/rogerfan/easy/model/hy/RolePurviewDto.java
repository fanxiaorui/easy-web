package com.rogerfan.easy.model.hy;

import org.mybatis.generator.gen.SuperBean;

public class RolePurviewDto extends SuperBean {
    private Long id;

    private Long roleId;

    private Long purviewId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getRoleId() {
        return roleId;
    }

    public void setRoleId(Long roleId) {
        this.roleId = roleId;
    }

    public Long getPurviewId() {
        return purviewId;
    }

    public void setPurviewId(Long purviewId) {
        this.purviewId = purviewId;
    }
}